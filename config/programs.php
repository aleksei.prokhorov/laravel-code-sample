<?php

return [
    'filter_key' => 'filter_options',
    'per_page_key' => 'per_page',
    'items_per_page_default' => 20,
    'max_amount' => 10000,
];
