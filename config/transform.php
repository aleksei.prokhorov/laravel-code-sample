<?php

$transform = [
    'view' => [
        'description' => [
            'handlers' => ['splitOptions'],
        ],
        'target_audience' => [
            'handlers' => [
                'splitOptions' => ['|']
            ],
            'afterHandlers' => true,
        ],
        'major' => [
            'handlers' => ['ucfirst'],
        ],
        'education_document' => [
            'handlers' => ['ucfirst'],
        ],
        'expected_learning_outcomes' => [
            'handlers' => ['splitOptions'],
        ],
        'staffing_number' => [
            'handlers' => [
                'numWord' => [['человек', 'человека', 'человек'], true]
            ],
        ],
        'total_hours' => [
            'handlers' => [
                'numWord' => [['час', 'часа', 'часов']]
            ],
        ],
        'selection_criteria' => [
            'handlers' => [
                'splitOptions' => [';']
            ],
        ],
        'online_activities_percentage' => [
            'handlers' => [
                'append' => ['%']
            ],
        ],
        'project_activity_percentage' => [
            'handlers' => [
                'append' => ['%']
            ],
        ],
        'employees_among_lecturers' => [
            'handlers' => [
                'append' => ['%']
            ],
        ],
        'in_business_school' => [
            'handlers' => ['yesNo']
        ],
        'discounts_available' => [
            'handlers' => ['yesNo']
        ],
        'vyshka_plus' => [
            'handlers' => ['yesNo']
        ],
        'tags' => [
            'handlers' => [
                'splitOptions' => ['|']
            ]
        ],
    ],
    'api' => [
        'tags' => [
            'handlers' => [
                'splitOptions' => ['|']
            ]
        ],
        'staffing_number' => [
            'handlers' => [
                'numWord' => [['человек', 'человека', 'человек']],
                'prepend' => ['Нормативная численность группы '],
            ]
        ],
        'classroom_hours' => [
            'handlers' => [
                'numWord' => [['час', 'часа', 'часов']]
            ]
        ],
        'online_activities_percentage' => [
            'handlers' => [
                'append' => ['%']
            ]
        ],
        'project_activity_percentage' => [
            'handlers' => [
                'append' => ['%']
            ]
        ],
        'admitted_students_current_year' => [
            'handlers' => [
                'numWord' => [['человек', 'человека', 'человек']]
            ]
        ],
        'graduate_students_current_year' => [
            'handlers' => [
                'numWord' => [['человек', 'человека', 'человек']]
            ]
        ],
        'graduate_students' => [
            'handlers' => [
                'numWord' => [['человек', 'человека', 'человек']]
            ]
        ],
        'education_pricing' => [
            'handlers' => [
                'numWord' => [['рубль', 'рубля', 'рублей']]
            ]
        ],
        'employees_among_lecturers' => [
            'handlers' => [
                'append' => ['%']
            ]
        ],
        'main_lecturer_1_percentage' => [
            'handlers' => [
                'append' => ['%']
            ]
        ],
        'main_lecturer_2_percentage' => [
            'handlers' => [
                'append' => ['%']
            ]
        ],
        'mentor_hours' => [
            'handlers' => [
                'numWord' => [['час', 'часа', 'часов']]
            ]
        ],
        'total_hours' => [
            'handlers' => [
                'numWord' => [['час', 'часа', 'часов']]
            ]
        ],
        'opening_year' => [
            'handlers' => ['parseInt']
        ],
        'in_business_school' => [
            'handlers' => ['parseBool']
        ],
        'active' => [
            'handlers' => ['parseBool']
        ],
        'discounts_available' => [
            'handlers' => ['parseBool']
        ],
        'vyshka_plus' => [
            'handlers' => ['parseBool'],
            'ignoreSpecificMethod' => true
        ],
    ]
];

$defaults = [
    'recruitment_program' => '',
    'corporate_partner' => '',
    'discount_types' => '',
    'discount_text' => '',
    'program_webpage' => '',
    'program_manager_name' => '',
    'program_manager_img' => '',
    'main_lecturer_by_hours_1' => '',
    'main_lecturer_1_img' => '',
    'main_lecturer_by_hours_2' => '',
    'main_lecturer_2_img' => '',
    'education_pricing' => '',
    'description' => [],
    'target_audience' => [],
    'expected_learning_outcomes' => [],
    'selection_criteria' => [],
];

foreach ($defaults as $serviceName => $default) {
    $transform['view'][$serviceName]['default'] = $default;
}

return $transform;
