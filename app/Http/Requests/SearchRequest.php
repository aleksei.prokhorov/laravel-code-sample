<?php

namespace App\Http\Requests;

use App\Modules\Program\Filter\SearchDto;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            //
        ];
    }

    /**
     * @return SearchDto
     */
    public function getSearchDto(): SearchDto
    {
        $perPage = $this->request->get(config('programs.per_page_key'), config('programs.items_per_page_default'));
        $filters = $this->request->get(config('programs.filter_key'), []);
        $query = '';

        if (isset($filters['query'])) {
            $query = $filters['query'];
            unset($filters['query']);
        }

        return new SearchDto($query, $filters, $perPage);
    }
}
