<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Modules\Program\Filter\ResponseFactory;
use App\Modules\Program\Filter\Searcher;
use App\Modules\Program\Transform\ProgramPreviewTransformer;
use App\Services\Program\FilterService;

class SearchController extends Controller
{
    protected FilterService $filterService;
    protected ProgramPreviewTransformer $previewTransformer;
    protected ResponseFactory $responseFactory;

    public function __construct(
        FilterService $filterService,
        ProgramPreviewTransformer $previewTransformer,
        ResponseFactory $responseFactory
    ) {
        $this->filterService = $filterService;
        $this->previewTransformer = $previewTransformer;
        $this->responseFactory = $responseFactory;
    }

    public function index(SearchRequest $request)
    {
        $searchDto = $request->getSearchDto();

        $programSearcher = (new Searcher($searchDto))
            ->paginate()
            ->setLimitedFields(ProgramPreviewTransformer::PREVIEW_FIELDS)
            ->performSearch();

        return $this->responseFactory->makeInitialSearchResponse($programSearcher, $searchDto);
    }

    public function search(SearchRequest $request)
    {
        $searchDto = $request->getSearchDto();

        $programSearcher = (new Searcher($searchDto))
            ->paginate()
            ->setLimitedFields(ProgramPreviewTransformer::PREVIEW_FIELDS)
            ->performSearch();

        return $this->responseFactory->makeAjaxSearchResponse($programSearcher, $searchDto);
    }
}
