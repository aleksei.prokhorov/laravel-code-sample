<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Division
 * @package App\Models
 *
 * @property Collection|null $managers
 * @see Division::getManagersAttribute()
 */
class Division extends Model
{
    public $table = 'divisions';

    protected $fillable = [
        'name',
        'pos',
    ];

    public function programs()
    {
        return $this->hasMany(Program::class, 'division_id');
    }

    /**
     * @return null
     */
    public function getManagersAttribute()
    {
        $managers = User::whereCanManage($this)->get();
        return $managers ?? null;
    }

    /**
     * @param $manager
     * @return bool
     */
    public function isAllowedForManager($manager): bool
    {
        $result = false;

        if ($manager) {
            if ($manager->can('division.manage.all')
                || (
                    $manager->can('division.manage.assigned')
                    && $manager->isAssignedToEntity($this)
                )
            ) {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * @param array $managersIds
     */
    public function allowToManage($managersIds = [])
    {
        $currentlyAllowed = User::whereCanManage($this)->get();
        $usersLosingAccess = $currentlyAllowed->whereNotIn('id', $managersIds);

        $usersGainingAccess = collect($managersIds)->diff($currentlyAllowed->pluck('id'));

        if ($usersLosingAccess->isNotEmpty()) {
            $usersLosingAccess->each(function (User $user) {
                $user->disallow('manage', $this);
                $this->programs->each(function (Program $program) use ($user) {
                    $user->disallow('manage', $program);
                });
            });
        }

        if ($usersGainingAccess->isNotEmpty()) {
            $managers = User::whereIn('id', $usersGainingAccess)->get();
            $managers->each(function (User $manager) {
                $manager->allow('manage', $this);
            });
        }
    }
}
