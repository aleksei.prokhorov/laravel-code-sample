<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Silber\Bouncer\Database\HasRolesAndAbilities;

/**
 * Class User
 * @package App/Models
 *
 * @property Collection $managedPrograms
 * @see User::getManagedProgramsAttribute()
 *
 * @property Collection $managedDivisions
 * @see User::getManagedDivisionsAttribute()
 */
class User extends Authenticatable
{
    use HasRolesAndAbilities, HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * @return Collection
     */
    public function getManagedProgramsAttribute(): Collection
    {
        $managedProgramsIds = $this->abilities()
            ->where('abilities.entity_type', Program::class)
            ->pluck('abilities.entity_id')->toArray();
        $managedPrograms = Program::whereIn('id', $managedProgramsIds)->get();

        return $managedPrograms;
    }

    /**
     * @return Collection
     */
    public function getManagedDivisionsAttribute(): Collection
    {
        $managedDivisionsIds = $this->abilities()
            ->where('abilities.entity_type', Division::class)
            ->pluck('abilities.entity_id')->toArray();
        $managedDivisions = Division::whereIn('id', $managedDivisionsIds)->get();

        return $managedDivisions;
    }

    /**
     * @param $entity
     * @return bool
     */
    public function isAssignedToEntity($entity): bool
    {
        return $entity
            && $this->can('manage', $entity);
    }

    /**
     * @param $query
     * @param $entity
     */
    public function scopeWhereCanManage($query, $entity)
    {
        $query->where(function ($query) use ($entity) {
            $query->whereHas('abilities', function ($query) use ($entity) {
                $query->where('abilities.entity_type', get_class($entity))
                    ->where('abilities.entity_id', $entity->id);
            });
        });
    }
}
