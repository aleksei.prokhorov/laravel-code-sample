<?php

namespace App\Models;

use App\Traits\ModelMeta;
use Illuminate\Database\Eloquent\Model;

class CustomField extends Model
{
    use ModelMeta;

    public $table = 'custom_fields';

    /**
     * The attributes that should be cast to native types
     *
     * @var array
     */
    protected $casts = [
        'meta' => 'json'
    ];

    protected $fillable = [
        'entity',
        'section',
        'service_name',
        'name',
        'description',
        'field_type',
        'default_value',
        'possible_values',
        'excel_column_name',
        'active',
        'pos',
        'column_number',
        'important'
    ];

    public static $fieldTypes = [
        'text' => 'Текст',
        'date' => 'Дата',
        'number' => 'Число',
        'select' => 'Выпадающий список',
        'multiselect' => 'Выпадающий список (мультивыбор)',
        'yes_no' => 'Переключатель (Да/Нет)',
        'image' => 'Изображение'
    ];

    public function program()
    {
        return $this->belongsToMany(Program::class, 'programs_custom_fields', 'custom_field_id', 'program_id')
            ->withPivot('id', 'value');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
