<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Ramsey\Uuid\Uuid;

/**
 * Class Program
 * @package App\Models
 *
 * @property User|null $manager
 * @see Program::getManagerAttribute()
 */
class Program extends Model
{
    const DEFAULT_PHOTO = '/images/default_photo.png';

    const MODE_UPDATE = 'update';
    const MODE_PUBLISH = 'publish';
    const MODE_PENDING = 'pending';

    const STATUS_NEW =          0;
    const STATUS_ACTIVE =       1;
    const STATUS_MODERATION =   2;
    const STATUS_ARCHIVE =      3;
    const STATUS_PENDING =      4;

    public static $statuses = [
        self::STATUS_NEW => 'Новая',
        self::STATUS_ACTIVE => 'Реализуется',
        self::STATUS_MODERATION => 'На модерации',
        self::STATUS_ARCHIVE => 'Архивирована',
        self::STATUS_PENDING => 'Ожидает переутверждения',
    ];

    public $table = 'programs';

    protected $fillable = [
        'name',
        'description',
        'approved',
        'uuid'
    ];

    public static $sections = [
        'main' => 'Основное',
        'common' => 'Общая информация',
        'additional' => 'Доп. информация',
        'teachers' => 'Преподаватели',
        'discounts' => 'Скидки',
        'statistics' => 'Статистика',
        'promo' => 'Промо',
    ];

    public static $seasons = [
        '1' => 'Осень',
        '2' => 'Зима',
        '3' => 'Весна',
        '4' => 'Лето'
    ];

    public static $seasonMonths = [
        '1' => ['09','10','11'],
        '2' => ['12','01','02'],
        '3' => ['03','04','05'],
        '4' => ['06','07','08']
    ];

    public static function boot()
    {
        parent::boot();

        static::updating(function (Program $program) {
            /** @var User $user */
            $user = Auth::user();
            $emailSent = false;

            if (!empty($user) && $user->isNotAn('admin')) {
                if (!$emailSent
                    && $program->getOriginal('status') !== self::STATUS_MODERATION
                    && $program->status === self::STATUS_MODERATION
                ) {
//                    Mail::to('sample@email.ru')->send(new ProgramConfirmationMessage($program));
                    $emailSent = true;

                }

                if (!$emailSent
                    && $program->getOriginal('status') === self::STATUS_ACTIVE
                    && $program->status === self::STATUS_ARCHIVE
                ) {
//                    Mail::to('sample@email.ru')->send(new ProgramUnpublishingMessage($program));
                    $emailSent = true;
                }

                if (!$emailSent
                    && $program->getOriginal('status') === self::STATUS_MODERATION
                    && $program->status === self::STATUS_ARCHIVE
                ) {
//                    Mail::to('sample@email.ru')->send(new ProgramCancellationMessage($program));
                    $emailSent = true;
                }
            }
        });

        static::created(function (Program $program) {
            /** @var User $user */
            $user = Auth::user();
            if (!empty($user) && $user->isNotAn('admin')) {
                $user->allow('manage', $program);
            }

            if ($program->status === self::STATUS_MODERATION) {
//                Mail::to('sample@email.ru')->send(new ProgramConfirmationMessage($program));
            }

            $program->setUUID();
        });
    }

    public function setUUID()
    {
        if (empty($this->uuid)) {
            $uniqueFieldsAndTime = json_encode([
                $this->name ?? '',
                $this->division_id ?? '',
                $this->major ?? '',
                $this->study_form ?? '',
                $this->total_hours ?? '',
                $this->classroom_hours ?? '',
                $this->recruitment_program ?? '',
                $this->created_at
            ]);

            $this->uuid = Uuid::uuid5(Uuid::NAMESPACE_OID, $uniqueFieldsAndTime);
            $this->save();
        }
    }

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id');
    }

    /**
     * @return User|null
     */
    public function getManagerAttribute(): ?User
    {
        $manager = User::whereCanManage($this)->first();
        return $manager ?? null;
    }

    public function getStatusNameAttribute()
    {
        return self::$statuses[$this->status] ?? '';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function customField()
    {
        return $this->belongsToMany(CustomField::class, 'programs_custom_fields', 'program_id', 'custom_field_id')
            ->withPivot('id', 'value');
    }

    /*
     * Условие "Программа активна"
     */
    public function scopeActive($query, $value = 1)
    {
        if ($value == 1) {
            return $query->where('status', self::STATUS_ACTIVE);
        } else {
            return $query->where('status', '!=', self::STATUS_ACTIVE);
        }
    }

    /*
     * Условие "Программа активна" или "Ожидает переутверждения"
     */
    public function scopeActiveOrPending($query)
    {
        return $query->whereIn('status', [
            self::STATUS_ACTIVE,
            self::STATUS_PENDING
        ]);
    }

    /*
     * Отсортированный массив секций для вывода
     */
    public static function getSections()
    {
        return self::$sections;
    }

    /**
     * @param $manager
     * @return bool
     */
    public function isAllowedForManager($manager): bool
    {
        $result = false;

        if ($manager) {
            if ($manager->can('program.manage.all')
                || $this->divisionIsAllowedForManager($manager)
            ) {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * @param $manager
     * @return bool
     */
    public function divisionIsAllowedForManager($manager): bool
    {
        return $manager
            && $this->division
            && $manager->can('manage', $this->division);
    }

    /**
     * @param false $managerId
     */
    public function allowToManage($managerId = false)
    {
        $currentlyAllowed = User::whereCanManage($this)->get();

        $currentlyAllowed->each(function (User $user) {
            $user->disallow('manage', $this);
        });

        if ($managerId && $manager = User::find($managerId)) {
            $manager->allow('manage', $this);
        }
    }
}
