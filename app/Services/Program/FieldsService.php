<?php

namespace App\Services\Program;

use App\Models\CustomField;
use App\Models\Program;
use App\Repository\ProgramRepository;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

/**
 * Class FieldsService
 *
 */
class FieldsService
{
    protected $programRepository;

    public function __construct(ProgramRepository $programRepository)
    {
        $this->programRepository = $programRepository;
    }

    /**
     * @param CustomField $customField
     * @return array|string|string[]
     */
    public function getCustomFieldPossibleValues(CustomField $customField)
    {
        $fieldValues = '';

        switch ($customField->field_type) {
            case "yes_no":
                $fieldValues = ['yes' => 'Да', 'no' => 'Нет'];
                break;
            case "text":
                $fieldValues = '';
                break;
            case "select":
            case "multiselect":
                $possibleValues = collect(explode('|', $customField->possible_values ?? ""));
                $fieldValues = $possibleValues->mapWithKeys(function ($value) {
                    return [$value => $value];
                })->filter()->toArray();
                break;
        }

        return $fieldValues;
    }

    /**
     * @param $model
     * @param array $fieldNames
     * @return array
     */
    public function getCustomFieldsValues($model, $fieldNames = []): array
    {
        $result = [];
        $customFields = $model->customField
            ?? $this->programRepository->getCustomFields();

        foreach ($customFields as $customField) {
            $serviceName = $customField->service_name;
            $modelCustomField = $model->customField->where('service_name', $serviceName)->first();

            if (!empty($modelCustomField)) {
                if (!$fieldNames
                    || (
                        $fieldNames
                        && in_array($serviceName, $fieldNames)
                    )
                ) {
                    $result[$serviceName] = $modelCustomField->pivot->value;
                }
            } else {
                $result[$serviceName] = '';
            }
        }

        return $result;
    }

    /**
     * @param $value
     * @return string
     */
    public function prepareStartDateValueToShow($value): string
    {
        $monthNumbers = collect(explode('|', $value))->map(function ($month) {
            return ltrim($month, '0');
        });

        $minMaxMonths = collect([
            getMonthName($monthNumbers->first()),
            getMonthName($monthNumbers->last())
        ])->filter()->unique()->join(' - ');

        if (!empty($minMaxMonths)) {
            $value = $minMaxMonths;
        }

        return $value;
    }

    /**
     * @param Program $model
     * @param $values
     * @return bool
     */
    protected function checkImportantFields(Program $model, $values): bool
    {
        // program fields check
        $mainFields = collect($model->getAttributes())
            ->except(['id', 'active', 'logo', 'uuid', 'created_at', 'updated_at'])
            ->keys()->toArray();

        foreach ($mainFields as $field) {
            if ($model->isDirty($field)) {
                return true;
            }
        }

        // custom fields check
        $importantFieldsNames = CustomField::where('important', 1)->active()
            ->pluck('service_name')->toArray();

        $programImportantValues = $this->getCustomFieldsValues($model, $importantFieldsNames);
        $diffFields = collect($values)->only($importantFieldsNames)->diffAssoc($programImportantValues);

        if ($diffFields->isNotEmpty()) {
            return true;
        }

        return false;
    }
}
