<?php

namespace App\Services\Program;

use App\Models\Division;
use App\Models\CustomField;
use App\Models\Program;
use Illuminate\Support\Str;

/**
 * Class FilterService
 *
 */
class FilterService
{
    protected array $mainFieldsList;
    protected array $customFieldsList;

    protected FieldsService $programFieldsService;

    public function __construct(FieldsService $programFieldsService)
    {
        $this->programFieldsService = $programFieldsService;
    }

    public function getValidateData()
    {
        $filterData = $this->getFilterData();

        $validateData = collect($filterData)->mapWithKeys(function ($data, $key) {
            $values = implode(',', array_keys($data['values']));
            return [$key => $values];
        })->toArray();

        return $validateData;
    }

    /**
     * @param array $filterOptions
     * @return array
     */
    public function getFilterData($filterOptions = []): array
    {
        $configuredFieldsList = config('filter.fields') ?? [];
        $filterData = [];

        foreach ($configuredFieldsList as $fieldName) {
            $method = 'prepare' . Str::studly($fieldName) . 'Data';

            if (method_exists($this, $method)) {
                $fieldData = call_user_func([$this, $method]);
            } elseif ($this->isMainField($fieldName)) {
                $fieldData = $this->prepareMainFieldData($fieldName);
            } elseif ($this->isCustomField($fieldName)) {
                $fieldData = $this->prepareCustomFieldData($fieldName);
            } else {
                $fieldData = ['name' => $fieldName, 'values' => []];
            }

            $fieldData['selection'] = $filterOptions[$fieldName] ?? false;

            $filterData[$fieldName] = $fieldData;
        }

        return $filterData;
    }

    protected function prepareCorporateProgramsData(): array
    {
        return [
            'name' => 'Корпоративные программы',
            'values' => ['yes' => 'Да', 'no' => 'Нет']
        ];
    }

    protected function prepareCampusData(): array
    {
        $cities = collect(config('possible_values.campus') ?? [])
            ->mapWithKeys(function ($value) {
                return [$value => $value];
            });

        return [
            'name' => 'Кампус',
            'values' => $cities->toArray()
        ];
    }

    protected function prepareVyshkaPlusData(): array
    {
        $customField = CustomField::where('service_name', 'vyshka_plus')->first();
        $fieldValues = $this->programFieldsService->getCustomFieldPossibleValues($customField);

        return [
            'name' => 'Только Вышка+',
            'values' => $fieldValues
        ];
    }

    protected function prepareTypeIdData(): array
    {
        $values = collect(['ПП', 'ПК', 'MBA', 'EMBA', 'DBA'])->mapWithKeys(function ($value) {
            return [$value => $value];
        })->toArray();

        return [
            'name' => 'Тип программы',
            'values' => $values
        ];
    }

    protected function prepareLevelData(): array
    {
        $customField = CustomField::where('service_name', 'target_audience')->first();
        $allPossibleValues = $this->programFieldsService->getCustomFieldPossibleValues($customField);

        $values = [];
        foreach ($allPossibleValues as $value) {
            preg_match('/(?<=\()(.+)(?=\))/i', $value, $matches);
            $preparedValue = $matches[0] ?? '';
            $parts = explode(',', $preparedValue);

            if (count($parts) > 1) {
                $levelValue = trim(explode('/', $parts[1])[0]);
                $values[$levelValue] = $levelValue;
            }
        }

        return [
            'name' => 'Уровень управления',
            'values' => $values
        ];
    }

    protected function prepareSizeData(): array
    {
        $customField = CustomField::where('service_name', 'target_audience')->first();
        $allPossibleValues = $this->programFieldsService->getCustomFieldPossibleValues($customField);

        $values = [];
        foreach ($allPossibleValues as $value) {
            preg_match('/(?<=\()(.+)(?=\))/i', $value, $matches);
            $preparedValue = $matches[0] ?? '';
            $parts = explode(',', $preparedValue);

            if (count($parts) > 1) {
                $sizeValue = trim(str_replace('организация', '', $parts[0]));
                $values[$sizeValue] = $sizeValue;
            }
        }

        return [
            'name' => 'Размер организации',
            'values' => $values
        ];
    }

    protected function prepareTagsData(): array
    {
        $customField = CustomField::where('service_name', 'tags')->first();
        $tagsStructure = collect($customField->tags_structure ?? [])
            ->filter(function ($tags) {
                return !empty($tags);
            })->toArray();

        return [
            'name' => 'Направления подготовки',
            'values' => $tagsStructure
        ];
    }

    protected function prepareDivisionIdData(): array
    {
        $divisions = Division::orderBy('name')->pluck('name', 'id')->toArray();

        return [
            'name' => 'Подразделение',
            'values' => $divisions
        ];
    }

    protected function prepareSeasonData(): array
    {
        return [
            'name' => 'Старт программы',
            'values' => Program::$seasons
        ];
    }

    /**
     * @param $fieldName
     * @return bool
     */
    protected function isMainField($fieldName): bool
    {
        if (empty($this->mainFieldsList)) {
            $this->mainFieldsList = ['name', 'description'];
        }

        return in_array($fieldName, $this->mainFieldsList);
    }

    /**
     * @param $fieldName
     * @return string[]
     */
    protected function prepareMainFieldData($fieldName): array
    {
        $mainFieldsNames = [
            'name' => 'Название',
            'description' => 'Описание преимуществ'
        ];

        return [
            'name' => $mainFieldsNames[$fieldName],
            'values' => ''
        ];
    }

    /**
     * @param $fieldName
     * @return bool
     */
    protected function isCustomField($fieldName): bool
    {
        if (empty($this->customFieldsList)) {
            $customFields = CustomField::select('service_name')->get();
            $this->customFieldsList = $customFields->pluck('service_name')->toArray();
        }

        return in_array($fieldName, $this->customFieldsList);
    }

    /**
     * @param $fieldName
     * @return array
     */
    protected function prepareCustomFieldData($fieldName): array
    {
        $fieldData = [];

        $customField = CustomField::where('service_name', $fieldName)->first();
        if (!empty($customField)) {
            $fieldValues = $this->programFieldsService->getCustomFieldPossibleValues($customField);

            $fieldData = [
                'name' => $customField->name,
                'values' => $fieldValues
            ];
        }

        return $fieldData;
    }
}
