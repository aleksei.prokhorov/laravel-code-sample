<?php

namespace App\Modules\Program\Filter;

use App\Models\CustomField;
use App\Models\Program;
use App\Modules\Program\Contracts\FilteringContract;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class ConditionsManager implements FilteringContract
{
    protected array $conditions = [];

    /**
     * ConditionsManager constructor.
     * @param SearchDto $dto
     */
    public function __construct(SearchDto $dto)
    {
        $optionsList = $dto->toArray();
        $this->setConditions($optionsList);
    }

    /**
     * @param array $optionsList
     */
    private function setConditions(array $optionsList = [])
    {
        foreach ($optionsList as $optionName => $option) {
            $method = 'prepare' . Str::studly($optionName) . 'Cnd';

            if (method_exists($this, $method)) {
                call_user_func([$this, $method], $option);
            } else {
                $this->conditions[$optionName] = $option;
            }
        }
    }

    /**
     * @param string $name
     * @return array|\ArrayAccess|mixed
     */
    public function getCondition(string $name)
    {
        return Arr::get($this->conditions, $name, []);
    }

    /**
     * @return array
     */
    public function getConditions(): array
    {
        return $this->conditions;
    }

    /**
     * @return bool
     */
    public function hasWordsCondition(): bool
    {
        return !empty($this->conditions['words']);
    }

    /**
     * @param Builder $query
     * @return FilteringContract
     */
    public function applyConditions(Builder $query): FilteringContract
    {
        if ($this->conditions) {
            foreach ($this->conditions as $cndName => $value) {
                $method = 'prepare' . Str::studly($cndName) . 'Query';

                if (method_exists($this, $method)) {
                    $query = call_user_func([$this, $method], $query, $value);
                } else {
                    if (is_array($value)) {
                        $query->whereIn($cndName, $value);
                    } else {
                        $query->where($cndName, $value);
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @param $value
     */
    private function prepareTagsGroupCnd($value)
    {
        $customField = CustomField::where('service_name', 'tags')->first();
        $tagsStructure = $customField->tags_structure;
        $this->conditions['tags'] = $tagsStructure[$value] ?? [];
    }

    /**
     * @param $value
     */
    private function prepareQueryCnd($value)
    {
        if ($value) {
            $cleanValue = preg_replace("/[^а-яёa-z,]/iu", ' ', mb_strtolower($value));
            $words = collect(explode(' ', $cleanValue));
            $words = $words->map(function ($word) {
                return trim($word);
            })->filter()->all();

            $this->conditions['words'] = $words ?? [];
        }
    }

    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    private function prepareWordsQuery($query, $value)
    {
        if ($value) {
            return $query->where(function ($query) use ($value) {
                foreach ($value as $word) {
                    $query->orWhere('name', 'LIKE', '%' . $word . '%');
//                    $query->orWhere('description', 'LIKE', '%' . $word . '%');
                    $query->orWhereHas('division', function ($query) use ($word) {
                        $query->where('name', 'LIKE', '%' . $word . '%');
                    });
                }

                $query->orWhereHas('customField', function ($query) use ($value) {
                    $query->where(function ($query) use ($value) {
                        $this->addCustomFieldOrWhereLikeCnd($query, 'tags', $value);
                        $this->addCustomFieldOrWhereLikeCnd($query, 'expected_learning_outcomes', $value);
                    });
                });
            });
        }

        return $query;
    }

    /**
     * @param $query
     * @param $values
     * @return mixed
     */
    private function prepareTagsQuery($query, $values)
    {
        if ($values) {
            $query = $this->addCustomFieldMultipleValuesOrWhereCnd($query, 'tags', $values);
        }

        return $query;
    }

    /**
     * @param $query
     * @param $values
     * @return mixed
     */
    private function prepareSizeQuery($query, $values)
    {
        if (!empty($values)) {
            $query->whereHas('customField', function ($query) use ($values) {
                $query->where('service_name', 'target_audience')
                    ->where(function ($query) use ($values) {
                        foreach ($values as $value) {
                            $query->orWhere('value', 'LIKE', '%' . $value . '%');
                        }
                    });
            });
        }

        return $query;
    }

    /**
     * @param $query
     * @param $values
     * @return mixed
     */
    private function prepareLevelQuery($query, $values)
    {
        if (!empty($values)) {
            $query->whereHas('customField', function ($query) use ($values) {
                $query->where('service_name', 'target_audience')
                    ->where(function ($query) use ($values) {
                        foreach ($values as $value) {
                            $query->orWhere('value', 'LIKE', '%' . $value . '%');
                        }
                    });
            });
        }

        return $query;
    }

    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    private function prepareCorporateProgramsQuery($query, $value)
    {
        if ($value) {
            $query->where(function ($query) use ($value) {
                $query->when($value == 'yes', function ($query) {
                    $query->whereHas('customField', function ($query) {
                        $query->where('service_name', 'corporate_partner')
                            ->whereNotNull('value')
                            ->where('value', '!=', '');
                    });
                }, function ($query) {
                    $query->whereDoesntHave("customField", function ($query) {
                        $query->where('service_name', 'corporate_partner');
                    })->orWhereHas('customField', function ($query) {
                        $query->where('service_name', 'corporate_partner')
                            ->where(function ($query) {
                                $query->whereNull('value')
                                    ->orWhere('value', '');
                            });
                    });
                });
            });
        }

        return $query;
    }

    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    private function prepareVyshkaPlusQuery($query, $value)
    {
        if ($value) {
            $query->where(function ($query) use ($value) {
                $query->when($value == 'yes', function ($query) {
                    $query->whereHas('customField', function ($query) {
                        $query->where('service_name', 'vyshka_plus')
                            ->where('value', 1);
                    });
                }, function ($query) {
                    $query->whereDoesntHave("customField", function ($query) {
                        $query->where('service_name', 'vyshka_plus');
                    })->orWhereHas('customField', function ($query) {
                        $query->where('service_name', 'vyshka_plus')
                            ->where('value', 0);
                    });
                });
            });
        }

        return $query;
    }

    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    private function prepareTypeIdQuery($query, $value)
    {
        if (!empty($value)) {
            $query->whereHas('customField', function ($query) use ($value) {
                $query->where('service_name', 'type_id');
                $query->where(function ($query) use ($value) {
                    foreach ($value as $item) {
                        if ($item == 'ПП') {
                            $query->orWhere('value', 'LIKE', 'ПП');
                            $query->orWhere('value', 'LIKE', 'Master in/Executive Master in/Doctor in');
                            $query->orWhere('value', 'LIKE', 'Master of Management in/Executive Master of Management in');
                        }
                        $query->orWhere('value', 'LIKE', $item);
                    }
                });
            });
        }

        return $query;
    }

    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    private function prepareStudyFormQuery($query, $value)
    {
        if ($value) {
            $query->whereHas('customField', function ($query2) use ($value) {
                $query2->where('service_name', 'study_form');
                $query2->where(function ($query3) use ($value) {
                    foreach ($value as $item) {
                        $query3->orWhere('value', 'LIKE', $item);
                    }
                });
            });
        }

        return $query;
    }

    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    private function prepareStudyFormatQuery($query, $value)
    {
        if ($value) {
            $query->whereHas('customField', function ($query2) use ($value) {
                $query2->where('service_name', 'study_format');
                $query2->where(function ($query3) use ($value) {
                    foreach ($value as $item) {
                        $query3->orWhere('value', 'LIKE', $item);
                    }
                });
            });
        }

        return $query;
    }

    /**
     * @param $query
     * @param $values
     * @return mixed
     */
    private function prepareSeasonQuery($query, $values)
    {
        if ($values) {
            $searchValues = [];
            // Convert season ids to season names
            foreach ($values as $k => $value) {
                if (!empty(Program::$seasonMonths) && isset(Program::$seasonMonths[$value])) {
                    $searchValues = array_merge($searchValues, Program::$seasonMonths[$value]);
                }
            }

            $fieldName = 'start_date';

            $query->whereHas('customField', function ($query2) use ($fieldName, $searchValues) {
                $query2->where('service_name', $fieldName);
                $query2->where(function ($query3) use ($searchValues) {
                    foreach ($searchValues as $item) {
                        $query3->orWhere('value', 'LIKE', '%.' . $item . '.%');
                    }
                });
            });
        }

        return $query;
    }

    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    private function prepareLanguageFormatQuery($query, $value)
    {
        if ($value) {
            $query->whereHas('customField', function ($query2) use ($value) {
                $query2->where('service_name', 'language_format');
                $query2->where(function ($query3) use ($value) {
                    foreach ($value as $item) {
                        $query3->orWhere('value', 'LIKE', $item);
                    }
                });
            });
        }

        return $query;
    }

    /**
     * @param $query
     * @param $fieldName
     * @param $values
     * @return mixed
     */
    private function addCustomFieldMultipleValuesOrWhereCnd($query, $fieldName, $values)
    {
        $query->whereHas('customField', function ($query2) use ($fieldName, $values) {
            $query2->where('service_name', $fieldName);
            $query2->where(function ($query3) use ($values) {
                foreach ($values as $item) {
                    $query3->orWhere('value', 'LIKE', $item);
                    $query3->orWhere('value', 'LIKE', $item . '|%');
                    $query3->orWhere('value', 'LIKE', '%|' . $item);
                    $query3->orWhere('value', 'LIKE', '%|' . $item . '|%');
                }
            });
        });

        return $query;
    }

    /**
     * @param $query
     * @param $fieldName
     * @param $value
     */
    private function addCustomFieldOrWhereLikeCnd($query, $fieldName, $value)
    {
        $query->orWhere(function ($query) use ($fieldName, $value) {
            $query->where('service_name', $fieldName);
            $query->where(function ($query) use ($value) {
                foreach ($value as $word) {
                    $query->orWhere('value', 'LIKE', '%' . $word . '%');
                }
            });
        });
    }
}
