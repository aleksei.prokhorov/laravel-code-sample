<?php

namespace App\Modules\Program\Filter;

use App\Models\Program;
use App\Modules\Program\Contracts\OrderingContract;
use App\Modules\Program\Contracts\QueryingContract;
use App\Modules\Program\Contracts\SearchingContract;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class QueryingManager implements QueryingContract
{
    private SearchingContract $searcher;
    private OrderingContract $orderingManager;

    public function __construct(SearchingContract $searcher, OrderingContract $orderingManager)
    {
        $this->searcher = $searcher;
        $this->orderingManager = $orderingManager;
    }

    /**
     * @return Builder
     */
    public function makeInitialQuery(): Builder
    {
        return Program::active()
            ->when(!empty($limitedFields), function ($query) {
                $query->with(['customField' => function ($query) {
                    $query->whereIn('service_name', $this->searcher->getLimitedFields());
                }]);
            }, function ($query) {
                $query->with('customField');
            });
    }

    /**
     * @return Builder
     */
    public function makeUpcomingQuery(): Builder
    {
        $upcomingQuery = $this->makeInitialQuery()
            ->whereHas('customField', function ($query) {
                $query->where('service_name', 'start_date')
                    ->where(DB::raw("STR_TO_DATE(value,'%d.%m.%Y')"), '>', Carbon::now());
            });

        $this->orderingManager->orderByStartDate($upcomingQuery);
        $upcomingQuery->limit(config('programs.max_amount'));

        return $upcomingQuery;
    }

    /**
     * @return Builder
     */
    public function makeOutdatedQuery(): Builder
    {
        $outdatedQuery = $this->makeInitialQuery()
            ->whereHas('customField', function ($query) {
                $query->where('service_name', 'start_date')
                    ->where(DB::raw("STR_TO_DATE(value,'%d.%m.%Y')"), '<', Carbon::now());
            });

        $this->orderingManager->orderByStartDate($outdatedQuery, 'desc');
        $outdatedQuery->limit(config('programs.max_amount'));

        return $outdatedQuery;
    }

    /**
     * @return Builder
     */
    public function makeDatelessQuery(): Builder
    {
        $datelessQuery = $this->makeInitialQuery()
            ->whereHas('customField', function ($query) {
                $query->where('service_name', 'start_date')
                    ->where(function ($query) {
                        $query->where('value', '=', '')
                            ->orWhereNull('value');
                    });
            });

        $this->orderingManager->orderByName($datelessQuery);
        $datelessQuery->limit(config('programs.max_amount'));

        return $datelessQuery;
    }
}
