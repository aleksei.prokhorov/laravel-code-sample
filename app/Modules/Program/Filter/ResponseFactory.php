<?php

namespace App\Modules\Program\Filter;

use App\Modules\Program\Contracts\SearchingContract;
use App\Services\Program\FilterService;
use App\Modules\Program\Transform\ProgramPreviewTransformer;
use Illuminate\Http\JsonResponse;

class ResponseFactory
{
    private FilterService $filterService;
    private ProgramPreviewTransformer $previewTransformer;

    public function __construct(FilterService $filterService, ProgramPreviewTransformer $previewTransformer)
    {
        $this->filterService = $filterService;
        $this->previewTransformer = $previewTransformer;
    }

    /**
     * @param SearchingContract $searcher
     * @param SearchDto $searchDto
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function makeInitialSearchResponse(SearchingContract $searcher, SearchDto $searchDto)
    {
        $filterData = $this->filterService->getFilterData($searchDto->toArray());

        return view('home', [
            'programs' => $searcher->getProgramsTransformedWith($this->previewTransformer),
            'links' => $searcher->getLinks(),
            'filterData' => $filterData,
            'searchQuery' => $searchDto->query,
            'title' => 'Фильтрация'
        ]);
    }

    /**
     * @param SearchingContract $searcher
     * @param SearchDto $searchDto
     * @return JsonResponse
     */
    public function makeAjaxSearchResponse(SearchingContract $searcher, SearchDto $searchDto): JsonResponse
    {
        $filterData = $this->filterService->getFilterData($searchDto->toArray());

        $response = [
            'status' => true,
            'htmlFilters' => view('client.includes.filter_form.all_filters', [
                'filterData' => $filterData
            ])->render(),
            'htmlSearch' => view('client.includes.search_input', [
                'value' => $searchDto->query
            ])->render(),
            'htmlCards' => view('client.includes.cards', [
                'programs' => $searcher->getProgramsTransformedWith($this->previewTransformer)
            ])->render(),
            'htmlPagination' => view('client.includes.pagination', [
                'links' => $searcher->getLinks(),
            ])->render(),
        ];

        return response()->json($response, 200, [], JSON_UNESCAPED_UNICODE);
    }
}
