<?php

namespace App\Modules\Program\Filter;

class SearchDto
{
    private string $query = '';

    private array $filters = [];

    private int $perPage;

    public function __construct($query, $filters, $perPage)
    {
        $this->query = $query;
        $this->filters = $filters;
        $this->perPage = $perPage ?? config('programs.items_per_page_default');
    }

    /**
     * @return string
     */
    public function getQuery(): string
    {
        return $this->query;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return array_merge(
            [
                'query' => $this->query
            ],
            $this->filters
        );
    }
}
