<?php

namespace App\Modules\Program\Filter;

use App\Models\Program;
use App\Modules\Program\Contracts\FilteringContract;
use App\Modules\Program\Contracts\OrderingContract;
use App\Modules\Program\Contracts\QueryingContract;
use App\Modules\Program\Contracts\SearchingContract;
use App\Modules\Program\Contracts\TransformingContract;
use Illuminate\Contracts\Pagination\LengthAwarePaginator as LengthAwarePaginatorInterface;
use Illuminate\Database\Query\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class Searcher implements SearchingContract
{
    private bool $paginate = false;
    private LengthAwarePaginatorInterface $paginator;

    private SearchDto $dto;
    private FilteringContract $conditionsManager;
    private OrderingContract $orderingManager;
    private QueryingContract $queryingManager;

    private array $limitedFields = [];
    private int $chunkSize = 20;
    private Builder $query;
    private Collection $programs;

    /**
     * Searcher constructor.
     * @param SearchDto $dto
     */
    public function __construct(SearchDto $dto)
    {
        $this->dto = $dto;
        $this->conditionsManager = new ConditionsManager($dto);
        $this->orderingManager = new OrderingManager();
        $this->queryingManager = new QueryingManager($this, $this->orderingManager);
        $this->programs = collect();
    }

    /**
     * @return SearchingContract
     */
    public function paginate(): SearchingContract
    {
        $this->paginate = true;
        return $this;
    }

    /**
     * @param array $limitedFields
     * @return SearchingContract
     */
    public function setLimitedFields(array $limitedFields = []): SearchingContract
    {
        $this->limitedFields = $limitedFields;
        return $this;
    }

    /**
     * @return array
     */
    public function getLimitedFields(): array
    {
        return $this->limitedFields;
    }

    /**
     * @param int $size
     * @return SearchingContract
     */
    public function setChunkSize(int $size = 20): SearchingContract
    {
        $this->chunkSize = $size;
        return $this;
    }

    /**
     * @param bool $callQuery
     * @return $this
     */
    public function performSearch(bool $callQuery = false): SearchingContract
    {
        if ($this->conditionsManager->hasWordsCondition()) {
            $this->orderProgramsAfterSelection();
        } else {
            $this->prepareQueryWithOrdering();
        }

        if ($this->paginate) {
            $this->paginatePrograms();
        }

        if ($this->programsHaveNotBeenObtainedYet() && $callQuery) {
            $this->programs = $this->query->get();
        }

        return $this;
    }

    private function orderProgramsAfterSelection()
    {
        $this->conditionsManager
            ->applyConditions($query = $this->queryingManager->makeInitialQuery());

        $this->programs = $this->orderingManager->orderPrograms(
            $query->get(),
            $this->conditionsManager->getConditions()
        );
    }

    private function prepareQueryWithOrdering()
    {
        $this->conditionsManager
            ->applyConditions($upcomingQuery = $this->queryingManager->makeUpcomingQuery())
            ->applyConditions($outdatedQuery = $this->queryingManager->makeOutdatedQuery())
            ->applyConditions($datelessQuery = $this->queryingManager->makeDatelessQuery());

        $this->query = $upcomingQuery->unionAll($outdatedQuery)->unionAll($datelessQuery);
    }

    /**
     * @return bool
     */
    private function programsHaveNotBeenObtainedYet(): bool
    {
        return $this->programs->isEmpty() && !empty($this->query);
    }

    /**
     * @param TransformingContract $transformer
     * @return Collection
     */
    public function getProgramsTransformedWith(TransformingContract $transformer): Collection
    {
        $transformer->setConditionsManager($this->conditionsManager);
        $transformedPrograms = collect();

        $this->pipePrograms(function ($programs) use ($transformer, &$transformedPrograms) {
            $transformedPrograms = $transformedPrograms->merge(
                $programs->map(function (Program $program) use ($transformer) {
                    return $transformer->transform($program);
                })
            );
        });

        return $transformedPrograms;
    }

    /**
     * @return Collection
     */
    public function getPrograms(): Collection
    {
        if ($this->programsHaveNotBeenObtainedYet()) {
            $this->programs = $this->query->get();
        }

        return $this->programs;
    }

    /**
     * @param callable $callback
     */
    public function pipePrograms(callable $callback)
    {
        if ($this->programsHaveNotBeenObtainedYet()) {
            $this->query->chunk($this->chunkSize, $callback);
        } else {
            $this->programs->chunk($this->chunkSize)->each($callback);
        }
    }

    /**
     * @param string $path
     * @param string $template
     * @return string
     */
    public function getLinks(string $path = '/search', string $template = 'pagination.provided_layout'): string
    {
        if ($this->paginate && !empty($this->paginator)) {
            return $this->paginator->setPath($path)
                ->withQueryString()->links($template);
        } else {
            return '';
        }
    }

    /**
     * @return int
     */
    public function getProgramsCount(): int
    {
        return $this->programs->count();
    }

    private function paginatePrograms()
    {
        $perPage = $this->dto->getPerPage();

        if ($this->programsHaveNotBeenObtainedYet()) {
            $this->paginator = $this->query->paginate($perPage);
        } else {
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            $this->paginator = new LengthAwarePaginator($this->programs, count($this->programs), $perPage, $currentPage);
            $this->programs = $this->programs->forPage($currentPage, $perPage);
        }
    }
}
