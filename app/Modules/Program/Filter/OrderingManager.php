<?php

namespace App\Modules\Program\Filter;

use App\Models\CustomField;
use App\Models\Program;
use App\Modules\Program\Contracts\OrderingContract;
use App\Modules\Program\Contracts\WeightingContract;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class OrderingManager implements OrderingContract
{
    private array $programsWeights = [];

    private WeightingContract $weightCalculator;

    public function __construct()
    {
        $this->weightCalculator = new WeightCalculator();
    }

    /**
     * @param Collection $programs
     * @param array $conditions
     * @return Collection
     */
    public function orderPrograms(Collection $programs, array $conditions): Collection
    {
        $this->setInitialProgramWeights($programs);

        if ($conditions) {
            foreach ($conditions as $cndName => $value) {
                $method = 'orderBy' . Str::studly($cndName);

                if (method_exists($this, $method)) {
                    call_user_func([$this, $method], $programs, $value);
                }
            }
        }

        return $programs->sortByDesc(function ($program) {
            return $this->programsWeights[$program->id];
        });
    }

    /**
     * @param Builder $query
     * @param string $direction
     * @return Builder
     */
    public function orderByName(Builder $query, string $direction = 'ASC'): Builder
    {
        $query->orderBy('name', $direction);
    }

    /**
     * @param Builder $query
     * @param string $direction
     * @return Builder
     */
    public function orderByStartDate(Builder $query, string $direction = 'ASC'): Builder
    {
        $query->orderBy(
            CustomField::selectRaw("STR_TO_DATE(value,'%d.%m.%Y')")
                ->join('programs_custom_fields', 'programs_custom_fields.custom_field_id', '=', 'custom_fields.id')
                ->whereColumn('programs_custom_fields.program_id', 'programs.id')
                ->where('service_name', 'start_date')
                ->take(1),
            $direction
        );
    }

    /**
     * @param $programs
     */
    private function setInitialProgramWeights($programs)
    {
        $this->programsWeights = $programs->mapWithKeys(function (Program $program) {
            return [$program->id => 0];
        });
    }

    /**
     * @param $programs
     * @param $value
     */
    private function orderByWords($programs, $value)
    {
        $programs->each(function ($program) use ($value) {
            $programWeight = $this->weightCalculator->calculateProgramWeight($program, $value);
            $this->programsWeights[$program->id] += $programWeight;
        });
    }
}
