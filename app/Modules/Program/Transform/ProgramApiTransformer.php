<?php

namespace App\Modules\Program\Transform;

/**
 * Class ProgramApiTransformer
 * @package App\Modules\Program\Transform
 */
class ProgramApiTransformer extends BaseProgramTransformer
{
    protected const DEFAULT = null;
    protected const DEFAULT_MULTIPLE = [];

    protected const CONFIG_SECTION = 'api';

    /**
     * @param $program
     */
    protected function transformSpecificFields($program)
    {
        $this->transformed['education_document_prepared'] = $this->transformField('education_document_prepared', $this->getCustomValue($program, 'education_document'));
        $this->transformed['certification_type_initial'] = mb_strtolower($this->getCustomValue($program, 'certification_type'));
    }

    /**
     * @param $fieldValue
     * @return string|null
     */
    protected function transformEducationDocumentField($fieldValue): ?string
    {
        $map = [
            'удостоверение' => 'Удостоверение о повышении квалификации НИУ ВШЭ',
            'диплом' => 'Диплом о профессиональной переподготовке НИУ ВШЭ',
        ];

        return $map[mb_strtolower($fieldValue)] ?? null;
    }

    /**
     * @param $fieldValue
     * @return string|null
     */
    protected function transformEducationDocumentPreparedField($fieldValue): ?string
    {
        $map = [
            'удостоверение' => 'удостоверение о повышении квалификации установленного НИУ ВШЭ образца',
            'диплом' => 'диплом о профессиональной переподготовке установленного НИУ ВШЭ образца',
        ];

        return $map[mb_strtolower($fieldValue)] ?? null;
    }
    /**
     * @param $fieldValue
     * @return array|mixed
     */
    protected function transformTargetAudienceField($fieldValue): array
    {
        preg_match_all("/\((.+)\)/U", $fieldValue, $matches);
        return $matches[1] ?? [];
    }

    /**
     * @param $fieldValue
     * @return mixed|null
     */
    protected function transformCertificationTypeField($fieldValue)
    {
        return mb_strtolower($fieldValue) == 'аккредитация';
    }

    /**
     * @param $fieldValue
     * @return string|null
     */
    protected function transformTypeIdField($fieldValue): ?string
    {
        $value = parent::transformTypeIdField($fieldValue);

        if (empty($value)) {
            $value = self::DEFAULT;
        }

        return $value;
    }

    /**
     * @param $fieldValue
     * @return string|null
     */
    protected function transformRecruitmentProgramField($fieldValue): ?string
    {
        $value = parent::transformRecruitmentProgramField($fieldValue);

        if (empty($value)) {
            $value = self::DEFAULT;
        }

        return $value;
    }
}
