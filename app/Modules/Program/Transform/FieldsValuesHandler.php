<?php

namespace App\Modules\Program\Transform;

use App\Modules\Program\Contracts\FieldsTransformHandlingContract;
use Illuminate\Support\Str;

class FieldsValuesHandler implements FieldsTransformHandlingContract
{
    /**
     * @param $configHandlers
     * @param $fieldValue
     * @return mixed
     */
    public function applyHandlers($configHandlers, $fieldValue)
    {
        foreach ($configHandlers as $key => $value) {
            list($handlerName, $params) = $this->parseHandler($key, $value);
            $fieldValue = $this->{$handlerName}($fieldValue, ...$params);
        }

        return $fieldValue;
    }

    /**
     * @param string $methodName
     * @param mixed ...$params
     * @return mixed
     */
    public function handleWithMethod(string $methodName, ...$params)
    {
        $methodName = 'handle' . Str::ucfirst($methodName);

        if (method_exists($this, $methodName)) {
            return $this->$methodName(...$params);
        } elseif (count($params) > 0) {
            return array_shift($params);
        } else {
            return '';
        }
    }

    /**
     * @param $fieldValue
     * @return string
     */
    public function handleYesNo($fieldValue): string
    {
        if (!empty($fieldValue)
            && (mb_strtolower($fieldValue) == 'да'
                || $fieldValue == 1
            )
        ) {
            $value = 'Да';
        } else {
            $value = 'Нет';
        }

        return $value;
    }

    /**
     * @param $fieldValue
     * @param false $delimiter
     * @return false|string[]
     */
    private function handleSplitOptions($fieldValue, $delimiter = false)
    {
        $value = !empty($fieldValue)
            ? $this->splitOptions($fieldValue, $delimiter)
            : $fieldValue;

        return $value;
    }

    /**
     * @param $fieldValue
     * @param $wordForms
     * @param false $divided
     * @return false|string|string[]
     */
    private function handleNumWord($fieldValue, $wordForms, $divided = false)
    {
        if (!empty($fieldValue) && is_numeric($fieldValue) && !empty($wordForms)) {
            $value = $this->numWord($fieldValue, $wordForms);
            if ($divided) {
                $value = explode(' ', $value);
            }
        } else {
            $value = $fieldValue;
        }

        return $value;
    }

    /**
     * @param $fieldValue
     * @return string|null
     */
    private function handleUcfirst($fieldValue): ?string
    {
        $value = !empty($fieldValue)
            ? Str::ucfirst($fieldValue)
            : $fieldValue;

        return $value;
    }

    /**
     * @param $fieldValue
     * @param $prefix
     * @return string|null
     */
    private function handlePrepend($fieldValue, $prefix): ?string
    {
        $value = !empty($fieldValue)
            ? $prefix . $fieldValue
            : $fieldValue;

        return $value;
    }

    /**
     * @param $fieldValue
     * @param $suffix
     * @return string|null
     */
    private function handleAppend($fieldValue, $suffix): ?string
    {
        $value = !empty($fieldValue)
            ? $fieldValue . $suffix
            : $fieldValue;

        return $value;
    }

    /**
     * @param $fieldValue
     * @return int|null
     */
    private function handleParseInt($fieldValue): ?int
    {
        $value = is_numeric($fieldValue)
            ? intval($fieldValue)
            : null;

        return $value;
    }

    /**
     * @param $fieldValue
     * @return bool
     */
    private function handleParseBool($fieldValue): bool
    {
        $value = mb_strtolower($fieldValue) == 'да'
            || $fieldValue == 1
            || $fieldValue === true;

        return $value;
    }

    /**
     * @param $key
     * @param $value
     * @return array
     */
    private function parseHandler($key, $value): array
    {
        if (is_numeric($key)) {
            $handler = $value;
            $params = [];
        } else {
            $handler = $key;
            $params = $value;
        }
        $handlerName = 'handle' . Str::ucfirst(Str::camel($handler));
        return [$handlerName, $params];
    }

    /**
     * @param $text
     * @param false $delimiter
     * @return false|string[]
     */
    private function splitOptions($text, $delimiter = false)
    {
        $values = explode($delimiter ?: PHP_EOL, $text);
        $values = array_filter($values);
        foreach ($values as &$value) {
            $value = trim($value);
        }
        return array_filter($values);
    }

    /**
     * @param $value
     * @param $words
     * @param bool $show
     * @return string
     */
    private function numWord($value, $words, $show = true): string
    {
        $num = $value % 100;
        if ($num > 19) {
            $num = $num % 10;
        }

        $out = ($show) ?  $value . ' ' : '';
        switch ($num) {
            case 1:  $out .= $words[0]; break;
            case 2:
            case 3:
            case 4:  $out .= $words[1]; break;
            default: $out .= $words[2]; break;
        }

        return $out;
    }
}
