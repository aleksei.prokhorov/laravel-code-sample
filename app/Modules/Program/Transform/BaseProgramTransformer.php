<?php

namespace App\Modules\Program\Transform;

use App\Models\CustomField;
use App\Models\Division;
use App\Models\Program;
use App\Modules\Program\Contracts\FieldsTransformHandlingContract;
use App\Modules\Program\Contracts\FilteringContract;
use App\Modules\Program\Contracts\TransformingContract;
use App\Repository\ProgramRepository;
use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Class BaseProgramTransformer
 * @package App\Transformers\Program
 */
abstract class BaseProgramTransformer implements TransformingContract
{
    protected const DEFAULT = '–';
    protected const DEFAULT_MULTIPLE = ['–'];

    protected const CONFIG_SECTION = 'view';

    protected FilteringContract $conditionsManager;

    protected FieldsTransformHandlingContract $fieldsValuesHandler;

    protected array $transformed = [];

    protected Collection $customFields;

    protected Program $program;

    /**
     * @var string[]
     */
    protected $mainFields = [
        'id',
        'active',
        'division_id',
        'name',
        'description',
    ];

    /**
     * @param FieldsTransformHandlingContract $fieldsValuesHandler
     */
    public function __construct(FieldsTransformHandlingContract $fieldsValuesHandler)
    {
        $this->fieldsValuesHandler = $fieldsValuesHandler;
    }

    /**
     * @param FilteringContract $conditionsManager
     */
    public function setConditionsManager(FilteringContract $conditionsManager)
    {
        $this->conditionsManager = $conditionsManager;
    }

    /**
     * @param Program $program
     * @return array
     */
    public function transform(Program $program): array
    {
        $this->program = $program;

        $this->prepareCustomFields();

        $this->prepareProgram();

        $this->transformMainFields();

        $this->transformCustomFields();

        if (method_exists($this, 'transformSpecificFields')) {
            $this->transformSpecificFields($this->program);
        }

        return $this->fetchTransformedData();
    }

    /**
     * @param array $program
     * @return array
     */
    public function transformArray(array $program): array
    {
        foreach ($program as $name => $value) {
            $this->transformed[$name] = $this->transformField($name, $value);
        }

        if (method_exists($this, 'transformSpecificFields')) {
            $this->transformSpecificFields($program);
        }

        return $this->fetchTransformedData();
    }

    /**
     * @param Collection|array $programs
     * @return array
     */
    public function transformMultiple($programs): array
    {
        $programsCollection = $programs instanceof Collection
            ? $programs
            : collect($programs);

        return $programsCollection->map(function ($program) {
            return $program instanceof Program
                ? $this->transform($program)
                : $this->transformArray($program);
        })->toArray();
    }

    /**
     * @param string $fieldName
     * @param $fieldValue
     * @return HigherOrderBuilderProxy|mixed|string|string[]
     */
    public function transformField(string $fieldName, $fieldValue)
    {
        if (in_array($fieldName, ['id', 'name', 'division_id'])) {
            return $fieldValue;
        }

        $fieldConfig = config('transform.' . static::CONFIG_SECTION . '.' . $fieldName) ?? [];
        $specificMethod = 'transform' . Str::ucfirst(Str::camel($fieldName)) . 'Field';

        if ($this->specificMethodShouldBeCalled('before', $specificMethod, $fieldConfig)) {
            return $this->{$specificMethod}($fieldValue);
        }

        if (!empty($fieldConfig['handlers'])) {
            $this->fieldsValuesHandler->applyHandlers($fieldConfig['handlers'], $fieldValue);
        }

        if (empty($fieldValue) && $fieldValue !== false) {
            $fieldValue = (!empty($fieldConfig) && isset($fieldConfig['default']))
                ? $fieldConfig['default']
                : $this->getDefaultValue($fieldName);
        }

        if ($this->specificMethodShouldBeCalled('after', $specificMethod, $fieldConfig)) {
            return $this->{$specificMethod}($fieldValue);
        }

        return $fieldValue;
    }

    protected function prepareCustomFields()
    {
        if (empty($this->customFields)) {
            $this->customFields = CustomField::all();
        }
    }

    protected function prepareProgram()
    {
        $this->program->loadMissing(['division', 'customField']);
    }

    protected function transformMainFields()
    {
        foreach ($this->mainFields as $fieldName) {
            $this->transformed[$fieldName] = $this->transformField($fieldName, $this->program->{$fieldName});
        }
    }

    protected function transformCustomFields()
    {
        foreach ($this->program->customField as $customField) {
            $serviceName = $customField->service_name;
            $customValue = $this->getCustomValue($this->program, $serviceName);
            $this->transformed[$serviceName] = $this->transformField($serviceName, $customValue);
        }
    }

    /**
     * @param $period
     * @param $methodName
     * @param $config
     * @return bool
     */
    protected function specificMethodShouldBeCalled($period, $methodName, $config): bool
    {
        if (!in_array($period, ['before', 'after'])) {
            return false;
        }

        return method_exists($this, $methodName)
            && !($this->ignoreSpecificMethod($config))
            && (
                ($period == 'before' && !$this->callAfterHandlers($config))
                ||
                ($period == 'after' && $this->callAfterHandlers($config))
            );
    }

    /**
     * @param array $fieldConfig
     * @return bool
     */
    protected function ignoreSpecificMethod(array $fieldConfig): bool
    {
        return isset($fieldConfig['ignoreSpecificMethod']) && $fieldConfig['ignoreSpecificMethod'];
    }

    /**
     * @param array $fieldConfig
     * @return bool
     */
    protected function callAfterHandlers(array $fieldConfig): bool
    {
        return isset($fieldConfig['afterHandlers']) && $fieldConfig['afterHandlers'];
    }

    /**
     * @return array
     */
    protected function fetchTransformedData(): array
    {
        $data = $this->transformed;
        $this->transformed = [];

        return $data;
    }

    /**
     * @param string $fieldName
     * @return HigherOrderBuilderProxy|mixed|string|string[]
     */
    protected function getDefaultValue(string $fieldName)
    {
        if (!empty($this->customFields)) {
            $customField = $this->customFields
                ->where('service_name', $fieldName)
                ->first();
        }

        if (empty($customField)) {
            $customField = CustomField::query()
                ->select(['field_type', 'default_value'])
                ->where('service_name', $fieldName)
                ->first();
        }

        if (!empty($customField)) {
            $emptyValue = $customField->field_type == 'multiselect'
                ? static::DEFAULT_MULTIPLE
                : static::DEFAULT;
            $defaultValue = $customField->default_value ?: $emptyValue;
        } else {
            $defaultValue = static::DEFAULT;
        }

        return $defaultValue;
    }

    /**
     * @param $division
     * @return string
     */
    protected function transformDivisionNameField($division): string
    {
        if (!empty($division) && !is_object($division)) {
            $division = Division::find($division);
        }

        $value = !empty($division)
            ? $division->name
            : '';

        return $value;
    }

    /**
     * @param $fieldValue
     * @return string|null
     */
    protected function transformRecruitmentProgramField($fieldValue): ?string
    {
        $value = !empty($fieldValue)
            ? Str::ucfirst($fieldValue)
            : '';

        return $value;
    }

    /**
     * @param $fieldValue
     * @return string
     */
    protected function transformVyshkaPlusField($fieldValue): string
    {
        $value = !empty($fieldValue)
            ? 'Входит в «Вышка+»'
            : 'Не входит в «Вышка+»';

        return $value;
    }

    /**
     * @param $fieldValue
     * @return mixed|null
     */
    protected function transformCertificationTypeField($fieldValue)
    {
        if (!empty($fieldValue)) {
            $value = mb_strtolower($fieldValue) == 'аккредитация'
                ? 'Да'
                : $fieldValue;
        } else {
            $value = static::DEFAULT;
        }

        return $value;
    }

    /**
     * @param $fieldValue
     * @return string|null
     */
    protected function transformStartDateField($fieldValue): ?string
    {
        $value = !empty($fieldValue)
            ? app(ProgramRepository::class)->prepareStartDateValueToShow($fieldValue)
            : static::DEFAULT;

        return $value;
    }

    /**
     * @param $fieldValue
     * @return string|null
     */
    protected function transformProgramManagerPositionField($fieldValue): ?string
    {
        $value = !empty($fieldValue)
            ? '(' . $fieldValue . ')'
            : static::DEFAULT;

        return $value;
    }

    /**
     * @param $fieldValue
     * @return string|null
     */
    protected function transformMainLecturerInfoField($fieldValue): ?string
    {
        $mainLecturerInfo = [];
        if (!empty($fieldValue['main_lecturer_position'])) {
            $mainLecturerInfo[] = '(' . $fieldValue['main_lecturer_position'] . ')';
        }
        if (!empty($fieldValue['main_lecturer_percentage'])) {
            $mainLecturerInfo[] = $fieldValue['main_lecturer_percentage'] . '%';
        }

        $value = !empty($mainLecturerInfo)
            ? implode(' - ', $mainLecturerInfo)
            : static::DEFAULT;

        return $value;
    }

    /**
     * @param $fieldValue
     * @return array
     */
    protected function transformTargetAudienceField($fieldValue): array
    {
        $mapping = config('transform-mapping.target_audience') ?? [];

        $value = collect($fieldValue)->map(function ($singleValue) use ($mapping) {
            foreach ($mapping as $search => $result) {
                if (strpos($singleValue, $search) === 0 ) {
                    return $result;
                }
            }
            return $singleValue;
        })->toArray();

        return $value;
    }

    /**
     * @param $fieldValue
     * @return string|null
     */
    protected function transformTypeIdField($fieldValue): ?string
    {
        $map = [
            'Master in/Executive Master in/Doctor in' => 'ПП',
            'Master of Management in/Executive Master of Management in' => 'ПП',
        ];

        return $map[$fieldValue] ?? $fieldValue ?? null;
    }

    /**
     * @param $fieldValue
     * @return false|string|string[]
     */
    protected function transformDurationField($fieldValue)
    {
        $value = static::DEFAULT;

        if (!empty($fieldValue)) {
            if (is_array($fieldValue)) {
                $number = $fieldValue['duration'];
                $unit = $fieldValue['duration_unit'];
            } else {
                $parts = explode('|', $fieldValue);
                $number = $parts[0];
                $unit = mb_strtolower($parts[1] ?? '');
            }

            if (!empty($number) && !empty($unit)) {
                $mapping = config('transform-mapping.duration', []);
                $value = $this->fieldsValuesHandler->handleWithMethod('numWord', $number, $mapping[$unit] ?? []);
            }
        }

        return $value;
    }

    /**
     * @param $program
     * @param $serviceName
     * @return string|null
     */
    protected function getCustomValue($program, $serviceName): ?string
    {
        if ($program instanceof Program) {
            $customField = $program->customField->where('service_name', $serviceName)->first();
            if (!empty($customField)) {
                if ($customField->field_type == 'image') {
                    $customValue = !empty($customField->pivot->image)
                        ? $customField->pivot->image->getPath()
                        : Program::DEFAULT_PHOTO;
                } else {
                    $customValue = $customField->pivot->value;
                }
            } else {
                $customValue = null;
            }
        } else {
            $customValue = $program[$serviceName] ?? null;
        }

        return $customValue;
    }
}
