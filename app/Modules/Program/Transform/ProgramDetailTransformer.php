<?php

namespace App\Modules\Program\Transform;

use App\Models\Program;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

/**
 * Class ProgramDetailTransformer
 * @package App\Modules\Program\Transform
 */
class ProgramDetailTransformer extends BaseProgramTransformer
{
    /**
     * @param Program|array $program
     */
    protected function transformSpecificFields($program)
    {
        if ($program instanceof Program) {
            /** @var User $user */
            $user = Auth::user();
            $this->transformed['allowedForUser'] = $user && $program->isAllowedForManager($user);
        } else {
            $this->transformed['allowedForUser'] = false;
        }

        $division = $program instanceof Program
            ? $program->division
            : $program['division'];
        $this->transformed['divisionName'] = $this->transformField('divisionName', $division);

        $this->transformed['main_lecturer_1_info'] = $this->transformField('main_lecturer_info', [
            'main_lecturer_position' => $this->getCustomValue($program, 'main_lecturer_1_position'),
            'main_lecturer_percentage' => $this->getCustomValue($program, 'main_lecturer_1_percentage'),
        ]);

        $this->transformed['main_lecturer_2_info'] = $this->transformField('main_lecturer_info', [
            'main_lecturer_position' => $this->getCustomValue($program, 'main_lecturer_2_position'),
            'main_lecturer_percentage' => $this->getCustomValue($program, 'main_lecturer_2_percentage'),
        ]);
    }
}
