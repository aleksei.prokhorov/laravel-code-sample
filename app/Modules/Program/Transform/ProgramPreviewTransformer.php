<?php

namespace App\Modules\Program\Transform;

use App\Models\CustomField;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

/**
 * Class ProgramPreviewTransformer
 * @package App\Modules\Program\Transform
 */
class ProgramPreviewTransformer extends BaseProgramTransformer
{
    protected const DEFAULT = '';
    protected const DEFAULT_MULTIPLE = [];

    public const PREVIEW_FIELDS = [
        'division_id',
        'type_id',
        'duration',
        'study_format',
        'target_audience',
        'tags',
        'start_date',
        'total_hours',
        'education_pricing'
    ];

    /**
     * @var string[]
     */
    protected $mainFields = [
        'id',
        'name',
    ];

    protected function prepareCustomFields()
    {
        if (empty($this->customFields)) {
            $this->customFields = CustomField::whereIn('service_name', self::PREVIEW_FIELDS)->get();
        }
    }

    protected function prepareProgram()
    {
        $this->program->loadMissing([
            'customField' => function ($query) {
                $query->whereIn('service_name', self::PREVIEW_FIELDS);
            }
        ]);
    }

    /**
     * @param $program
     */
    protected function transformSpecificFields($program)
    {
        /** @var User $user */
        $user = Auth::user();
        $this->transformed['allowedForUser'] = $user && $program->isAllowedForManager($user);

        $this->transformed['level'] = $this->transformField('level', $this->getCustomValue($program, 'target_audience'));
        $this->transformed['division_id'] = $program->division_id;
        $this->transformed['campus'] = $this->transformField('campus', $program->division->name ?? '');
    }

    /**
     * @param $fieldValue
     * @return string|null
     */
    protected function transformStudyFormatField($fieldValue): ?string
    {
        $map = [
            'Очный' => 'Очное обучение',
            'Смешанный' => 'Смешанное обучение',
            'Дистанционный' => 'Дистанционное обучение',
            'Онлайн' => 'Онлайн обучение'
        ];

        return $map[$fieldValue] ?? null;
    }

    /**
     * @param $fieldValue
     * @return array
     */
    protected function transformLevelField($fieldValue): array
    {
        preg_match_all("/\(.+, (.+)\)/U", $fieldValue, $matches);
        $levels = collect($matches[1] ?? [])->unique();

        $levelCondition = $this->conditionsManager->getCondition('target_audience.level');

        $firstFoundLevel = $levels->intersect($levelCondition)->first()
            ?? $levels->first();

        return [
            'main' => Str::ucfirst($firstFoundLevel),
            'other' => $levels->diff([$firstFoundLevel])->map(function ($level) {
                return Str::ucfirst($level);
            })->toArray()
        ];
    }

    /**
     * @param $fieldValue
     * @return string
     */
    protected function transformCampusField($fieldValue): string
    {
        $moscow = 'Москва';
        $allCities = collect(config('possible_values.campus'))->diff([$moscow]);

        foreach ($allCities as $city) {
            if (Str::startsWith($fieldValue, $city)) {
                return $city;
            }
        };

        return $moscow;
    }
}
