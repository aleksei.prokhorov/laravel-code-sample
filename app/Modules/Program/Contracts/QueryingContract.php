<?php

namespace App\Modules\Program\Contracts;

use Illuminate\Database\Query\Builder;

interface QueryingContract
{
    /**
     * @return Builder
     */
    public function makeInitialQuery(): Builder;

    /**
     * @return Builder
     */
    public function makeUpcomingQuery(): Builder;

    /**
     * @return Builder
     */
    public function makeOutdatedQuery(): Builder;

    /**
     * @return Builder
     */
    public function makeDatelessQuery(): Builder;
}
