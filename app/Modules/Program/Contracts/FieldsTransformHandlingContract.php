<?php

namespace App\Modules\Program\Contracts;

interface FieldsTransformHandlingContract
{
    /**
     * @param $configHandlers
     * @param $fieldValue
     * @return mixed
     */
    public function applyHandlers($configHandlers, $fieldValue);

    /**
     * @param string $methodName
     * @param mixed ...$params
     * @return mixed
     */
    public function handleWithMethod(string $methodName, ...$params);
}
