<?php

namespace App\Modules\Program\Contracts;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

interface OrderingContract
{
    /**
     * @param Collection $programs
     * @param array $conditions
     * @return Collection
     */
    public function orderPrograms(Collection $programs, array $conditions): Collection;

    /**
     * @param Builder $query
     * @param string $direction
     * @return Builder
     */
    public function orderByName(Builder $query, string $direction = 'ASC'): Builder;

    /**
     * @param Builder $query
     * @param string $direction
     * @return Builder
     */
    public function orderByStartDate(Builder $query, string $direction = 'ASC'): Builder;
}
