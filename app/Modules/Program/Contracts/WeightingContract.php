<?php

namespace App\Modules\Program\Contracts;

interface WeightingContract
{
    /**
     * @param $program
     * @param $words
     * @return int
     */
    public function calculateProgramWeight($program, $words): int;
}
