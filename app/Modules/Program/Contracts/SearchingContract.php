<?php

namespace App\Modules\Program\Contracts;

use Illuminate\Support\Collection;

interface SearchingContract
{
    /**
     * @return SearchingContract
     */
    public function paginate(): SearchingContract;

    /**
     * @param array $limitedFields
     * @return SearchingContract
     */
    public function setLimitedFields(array $limitedFields = []): SearchingContract;

    /**
     * @return array
     */
    public function getLimitedFields(): array;

    /**
     * @param int $size
     * @return SearchingContract
     */
    public function setChunkSize(int $size): SearchingContract;

    /**
     * @param bool $callQuery
     * @return SearchingContract
     */
    public function performSearch(bool $callQuery): SearchingContract;

    /**
     * @param TransformingContract $transformer
     * @return Collection
     */
    public function getProgramsTransformedWith(TransformingContract $transformer): Collection;

    /**
     * @return Collection
     */
    public function getPrograms(): Collection;

    /**
     * @param callable $callback
     * @return mixed
     */
    public function pipePrograms(callable $callback);

    /**
     * @return int
     */
    public function getProgramsCount(): int;

    /**
     * @param string $path
     * @param string $template
     * @return string
     */
    public function getLinks(string $path = '', string $template = ''): string;
}
