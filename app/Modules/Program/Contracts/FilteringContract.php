<?php

namespace App\Modules\Program\Contracts;

use Illuminate\Database\Query\Builder;

interface FilteringContract
{
    /**
     * @param string $name
     * @return array|\ArrayAccess|mixed
     */
    public function getCondition(string $name);

    /**
     * @return array
     */
    public function getConditions(): array;

    /**
     * @return bool
     */
    public function hasWordsCondition(): bool;

    /**
     * @param Builder $query
     * @return FilteringContract
     */
    public function applyConditions(Builder $query): FilteringContract;
}
