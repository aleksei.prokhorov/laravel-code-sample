<?php

namespace App\Modules\Program\Contracts;

use App\Models\Program;
use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Illuminate\Support\Collection;

interface TransformingContract
{
    /**
     * @param FilteringContract $conditionsManager
     */
    public function setConditionsManager(FilteringContract $conditionsManager);

    /**
     * @param Program $program
     * @return array
     */
    public function transform(Program $program): array;

    /**
     * @param array $program
     * @return array
     */
    public function transformArray(array $program): array;

    /**
     * @param Collection|array $programs
     * @return array
     */
    public function transformMultiple($programs): array;

    /**
     * @param string $fieldName
     * @param $fieldValue
     * @return HigherOrderBuilderProxy|mixed|string|string[]
     */
    public function transformField(string $fieldName, $fieldValue);
}
