<?php

namespace App\Modules\Program;

use App\Models\CustomField;
use App\Models\Program;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class TagsGroupAggregator
{
    private $possibleValues;
    private $tags;

    private array $countByTag = [];
    private array $countByType = [];
    private array $countByCampus = [];
    private array $countByStudyFormat = [];

    private int $programsCount = 0;

    public function __construct($tagsGroup)
    {
        $tagsField = CustomField::where('service_name', 'tags')->first();
        $tagsStructure = $tagsField->tags_structure;

        $this->tags = $tagsStructure[$tagsGroup] ?? [];
        $this->setPossibleValues();
    }

    public function setPossibleValues()
    {
        $customFields = CustomField::select('service_name', 'possible_values')
            ->whereIn('service_name', ['type_id', 'study_format'])
            ->get();

        $this->possibleValues = $customFields->mapWithKeys(function ($customField) {
            return [$customField->service_name => explode('|', $customField->possible_values)];
        })->toArray();

        $this->possibleValues['campus'] = config('possible_values.campus');
    }

    /**
     * @param $tags
     * @return mixed
     */
    public static function getMostCorrelatedTagsGroup($tags)
    {
        $tagsField = CustomField::where('service_name', 'tags')->first();
        $tagsStructure = $tagsField->tags_structure;

        $tagsGroupsCorrelation = collect($tagsStructure)->mapWithKeys(function ($groupTags, $groupName) use ($tags) {
            return [$groupName => collect($groupTags)->intersect($tags)->count()];
        });

        return $tagsGroupsCorrelation->sortDesc()->keys()->first();
    }

    /**
     * @return array
     */
    public function getInfo(): array
    {
        return [
            'tags' => $this->tags,
            'tagsCount' => $this->countByTag,
            'typesCount' => $this->countByType,
            'campusesCount' => $this->countByCampus,
            'studyFormatCount' => $this->countByStudyFormat,
            'programsCount' => $this->programsCount
        ];
    }

    /**
     * @param Collection $programs
     */
    public function handlePrograms(Collection $programs)
    {
        $programs->each(function (Program $program) {
            $this->handleProgram($program);
        });
    }

    /**
     * @param Program $program
     */
    private function handleProgram(Program $program)
    {
        $this->checkProgramTags($program);
        $this->checkProgramType($program);
        $this->checkProgramCampus($program);
        $this->checkProgramStudyFormat($program);

        $this->programsCount++;
    }

    /**
     * @param Program $program
     */
    private function checkProgramTags(Program $program)
    {
        $tagsField = $program->customField->firstWhere('service_name', 'tags');

        if (!empty($tagsField)) {
            $tags = explode('|', $tagsField->pivot->value);

            foreach ($this->tags as $tag) {
                if (in_array($tag, $tags)) {
                    $this->countByTag[$tag] = ($this->countByTag[$tag] ?? 0) + 1;
                }
            }
        }
    }

    /**
     * @param Program $program
     */
    private function checkProgramType(Program $program)
    {
        $typeField = $program->customField->firstWhere('service_name', 'type_id');

        if (!empty($tagsField)) {
            $typeId = $typeField->pivot->value;

            if (in_array($typeId, ['ПП', 'Master in/Executive Master in/Doctor in', 'Master of Management in/Executive Master of Management in'])) {
                $type = 'Профессиональная переподготовка';
            } elseif ($typeId == 'ПК') {
                $type = 'Повышение квалификации';
            } else {
                $type = 'MBA';
            }

            if (in_array($type, $this->possibleValues['type_id'])) {
                $this->countByType[$type] = ($this->countByType[$type] ?? 0) + 1;
            }
        }
    }

    /**
     * @param Program $program
     */
    private function checkProgramCampus(Program $program)
    {
        $programDivision = $program->division->name ?? '';

        $campus = 'Москва';
        foreach (config('possible_values.campus') as $value) {
            if (Str::startsWith($programDivision, $value)) {
                $campus = $value;
            }
        }

        if (in_array($campus, $this->possibleValues['campus'])) {
            $this->countByCampus[$campus] = ($this->countByCampus[$campus] ?? 0) + 1;
        }
    }

    /**
     * @param Program $program
     */
    private function checkProgramStudyFormat(Program $program)
    {
        $studyFormatField = $program->customField->firstWhere('service_name', 'study_format');

        $studyFormat = $studyFormatField->pivot->value ?? '';

        if (in_array($studyFormat, $this->possibleValues['study_format'])) {
            $this->countByStudyFormat[$studyFormat] = ($this->countByStudyFormat[$studyFormat] ?? 0) + 1;
        }
    }
}
