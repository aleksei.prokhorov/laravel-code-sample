<?php

if (!function_exists('lowerTrimmed')) {
    function lowerTrimmed($string)
    {
        return mb_strtolower(trim($string));
    }
}

if (!function_exists('getMonthNames')) {
    function getMonthNames()
    {
        return $monthNames = [
            'январь' =>     1,
            'февраль' =>    2,
            'март' =>       3,
            'апрель' =>     4,
            'май' =>        5,
            'июнь' =>       6,
            'июль' =>       7,
            'август' =>     8,
            'сентябрь' =>   9,
            'октябрь' =>   10,
            'ноябрь' =>    11,
            'декабрь' =>   12,
        ];
    }
}

if (!function_exists('getMonthName')) {
    function getMonthName($monthNumber)
    {
        $monthNames = getMonthNames();
        $flippedMonths = array_flip($monthNames);

        $monthName = false;
        if (isset($flippedMonths[$monthNumber])) {
            $monthName = $flippedMonths[$monthNumber];
        }

        return $monthName;
    }
}
