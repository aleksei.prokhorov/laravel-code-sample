<?php

namespace App\Traits;

use Illuminate\Support\Facades\Log;

/**
 * Trait ModelMeta
 *
 */
trait ModelMeta
{
    protected $metaColName = 'meta';

    public function hasAttribute($key)
    {
        return !!$this->getAttributeFromArray($key) || in_array($key, $this->fillable) || in_array($key, $this->dates) || in_array($key, $this->casts) || ($key == self::CREATED_AT) || ($key == self::UPDATED_AT);
    }

    public function __get($key)
    {
        if (!$this->hasAttribute($key)) {
            $meta = parent::__get($this->metaColName);
            if (isset($meta[$key])) {
                return $meta[$key];
            }
        }
        return parent::__get($key);
    }

    public function __isset($key)
    {
        if (!$this->hasAttribute($key)) {
            if (isset($this->{$this->metaColName}[$key])) {
                return true;
            }
        }
        return parent::__isset($key);
    }

    public function __set($key, $value)
    {
        if (!$this->hasAttribute($key)) {
            if (!in_array($key, config('custom_field_properties.meta'))) {
                $logValue = $value;
                if (is_array($logValue) || is_object($logValue)) {
                    $logValue = '[' . implodeRecursively((array)$logValue, ', ') . ']';
                }
                Log::alert('Not declared meta field: ' . 'key => ' . $key . '; value => ' . $logValue);
            }

            if (is_array($this->{$this->metaColName})) {
                $val = $this->{$this->metaColName};
                $val[$key] = $value;
                parent::__set($this->metaColName, $val);
            } else {
                parent::__set($this->metaColName, [$key => $value]);
            }
            return;
        }
        parent::__set($key, $value);
    }

    public function toArray()
    {
        /**
         * Конвертируем мету в основные свойства
         */
        $result = parent::toArray();
        if (is_array($this->{$this->metaColName})) {
            foreach ($this->{$this->metaColName} as $key => $val) {
                $result[$key] = $val;
            }
            unset($result[$this->metaColName]);
        }
        return $result;
    }
}
